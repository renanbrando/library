package library;

import javax.swing.JOptionPane;

public class JFind {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	
	private String search;
	
	public JFind(LoanableLibrary library) {
		// TODO Auto-generated constructor stub
		this.findManual();
		this.findManual(library);
	}
	
	public void findManual(){
		this.search = JOptionPane.showInputDialog(null, "Type the book's name:", "Search", JOptionPane.DEFAULT_OPTION);
	}
	
	public void findManual(LoanableLibrary library){
		LoanableManual result;
		Object[] books = library.searchManuals(this.search);
	    
	    result = (LoanableManual) JOptionPane.showInputDialog(null, "Listing all books...", "Books", JOptionPane.QUESTION_MESSAGE,
	        null, books, "Books");
	    
	    if (result != null){
	    	JOptionPane.showMessageDialog(null, result.toString(result));
	    }
	    else{
	    	JOptionPane.showMessageDialog(null, "Book not selected!");
	    }

	}
}
