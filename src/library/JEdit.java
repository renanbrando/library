package library;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class JEdit {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	
	private JTextField title;
	private JTextField author;
	private JTextField available;
	private JTextField borrower;
	private JTextField quantity;
	private String search;
	private LoanableManual result;
	
	public JEdit(LoanableLibrary library) {
		// TODO Auto-generated constructor stub
		title = new JTextField();
		author = new JTextField();
		available = new JTextField();
		borrower = new JTextField();
		quantity = new JTextField();
		this.editManual();
		this.editManual(library);
	}
	
	public void editManual(){
		this.search = JOptionPane.showInputDialog(null, "Type the book's name:", "Edit", JOptionPane.DEFAULT_OPTION);
	}

	public void editManual(LoanableLibrary library){
		Object[] books = library.searchManuals(this.search);
	    
	    this.result = (LoanableManual) JOptionPane.showInputDialog(null, "Listing all books...", "Books", JOptionPane.QUESTION_MESSAGE,
	        null, books, "Books");
	    
	    if (this.result != null){
	    	this.initEdition(this.title, this.author, this.available, this.borrower, this.quantity, result);
	    	Object[] message = {
				    "Title:", this.title,
				    "Author:", this.author,
				    "Available:", this.available,
				    "Borrower:", this.borrower,
				    "Quantity:", this.quantity
		};
		int option = JOptionPane.showConfirmDialog(null, message, "Edit Book", JOptionPane.OK_CANCEL_OPTION);
			if (option == JOptionPane.OK_OPTION) {
				if (this.checkFields(this.title.getText(), this.author.getText(), this.borrower.getText(), this.quantity.getText())){
					result.setTitle(this.title.getText());
					result.setAuthor(this.author.getText());
					result.setAvailable(this.available.getText().toLowerCase().equals("yes") ? true : false);
					result.setBorrower(this.borrower.getText());
					result.setQtd(getInt(this.quantity));
					JOptionPane.showMessageDialog(null, "Changes made successfully!");
				}
				else{
					JOptionPane.showMessageDialog(null, "Fill in all the fields properly!");
				}
			}		
	    }
	    else{
	    	JOptionPane.showMessageDialog(null, "No changes were made!");
	    }
	
	}
	
	public void initEdition(JTextField title, JTextField author, JTextField available, JTextField borrower, JTextField quantity, LoanableManual manual){
		title.setText(manual.getTitle());
		author.setText(manual.getAuthor());
		available.setText(String.valueOf(manual.isAvailable() ? "Yes" : "No"));
		borrower.setText(manual.getBorrower());
		quantity.setText(String.valueOf(manual.getQtd()));
	}
	
	public int getInt(JTextField field){
		int number = 0;
		try{
			  return Integer.parseInt(field.getText());
			} 
		catch (NumberFormatException e) {
			  JOptionPane.showMessageDialog(null, "Quantity should be a number!");
		};
		return number;
	}
	
	public boolean checkFields(String title, String author, String borrower, String quantity){
		if (title.isEmpty() || author.isEmpty() || borrower.isEmpty() || quantity.isEmpty() || !this.isNumeric(quantity))
			return false;
		return true;
	}
	
	//Check if is a number
	public boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
}
