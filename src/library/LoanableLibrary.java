package library;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class LoanableLibrary {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	private ArrayList<LoanableManual> library;

	
	//
	public LoanableLibrary() {
		// TODO Auto-generated constructor stub
		this.setLibrary(new ArrayList<LoanableManual>());
	}
	
	//Get a manual from the library
	public LoanableManual findManual(String serial){
		// TODO Auto-generated method stub
		for (LoanableManual m : this.getLibrary()){
			if (serial.equals(m.getSerialNumber()))
				return m;
		}
		return new LoanableManual();
	}

	//Add a manual
	public void addManual(LoanableManual manual) {
		// TODO Auto-generated method stub
		for (LoanableManual m : this.getLibrary()){
			if (m.getTitle().equals(manual.getTitle())){
				m.setQtd(manual.getQtd() + m.getQtd());
				return;
			}
		}
		this.getLibrary().add(manual);
		this.sort();
	}
		
	//Remove a manual
	public boolean removeManual(LoanableManual manual) {
		this.getLibrary().remove(manual);
		return true;
	}
	
	//Edit a manual
	public boolean editManual(LoanableManual manual){
		LoanableManual tmp = new LoanableManual();
		tmp =  tmp.ask();
		manual.setAuthor(tmp.getAuthor());
		manual.setTitle(tmp.getTitle());
		manual.setQtd(tmp.getQtd());
		manual.setBorrower(tmp.getBorrower());
		manual.setAvailable(tmp.isAvailable());
		return true;
	}
	
	//Search manuals
	public Object[] searchManuals(String search){
		Object[] books = new Object[this.library.size()];
		int i = 0;
	    for (LoanableManual l : this.library){
	    	if (l.getTitle().toLowerCase().contains(search.toLowerCase())){
	    		books[i] = l;
	    	}
	    	else{
	    		continue;
	    	}
	    	i++;
	    }
	    return books;
	}
	
	//Display all available manuals
	public void displayAllAvailableManuals() {
		// TODO Auto-generated method stub
		int i = 1;
		for (LoanableManual m: this.getLibrary()){
			if(m.isAvailable()){
				System.out.println("---------Manual number "+i+"-----------");
				m.printManual(m.getSerialNumber());
				i++;
			}
		}
	}
	
	//Display all available manuals
	public void displayAllNotAvailableManuals() {
		// TODO Auto-generated method stub
		int i = 1;
		for (LoanableManual m: this.getLibrary()){
			if(!m.isAvailable()){
				System.out.println("---------Manual number "+i+"-----------");
				m.printManual(m.getSerialNumber());
				i++;
			}
		}
	}
			
	//Display all manuals available or not
	public void displayAllManuals(boolean available) {
		// TODO Auto-generated method stub
		int i = 1;
		for (LoanableManual m: this.getLibrary()){
			if(m.isAvailable() == available){
				System.out.println("---------Manual number "+i+"-----------");
				m.printManual(m.getSerialNumber());
				i++;
			}
		}
	}
	
	//Display all manuals available or not
	public void displayAllManuals() {
	// TODO Auto-generated method stub
	int i = 1;
	for (LoanableManual m: this.getLibrary()){
		System.out.println("---------Manual number "+i+"-----------");
			m.printManual(m.getSerialNumber());
			i++;			}
	}
			
	//Return manual like a string
	public String toString(LoanableManual manual) {
		// TODO Auto-generated method stub
		String m = null;
		m = "Serial: " + manual.getSerialNumber() + "\n";
		m += "Title: " + manual.getTitle() + "\n";
		m += "Author: " + manual.getAuthor() + "\n";
		m += "Copies: " + manual.getQtd() + "\n";
		m += "Available: ";
		m += manual.isAvailable()? "Yes\n" : "No\n";
		m += "Borrower: " + manual.getBorrower() + "\n";
		return m;
	}
	
	//Validates entry
	public boolean validadesEntry(LoanableManual manual) {
		// TODO Auto-generated method stub
		for (LoanableManual m: this.getLibrary()){
			if (manual.getSerialNumber().equals(m.getSerialNumber())){
				System.out.println("The manual you are trying to add is not valid or already is in the library!");
				return false;
			}
		}
		this.addManual(manual);
		System.out.println("Manual added successfully!");
		return true;
	}
	
	//Borrow a manual
	public boolean borrowManual(LoanableManual manual){
		//Find a manual using its serial number and then try to decrease it quantity from the library
		if (manual.decreaseQtd(this.findManual(manual.getSerialNumber()))){
			manual.setAvailable(false);
			return true;
		}
		return false;
	}
	
	//Return a manual 
	public boolean returnManual(LoanableManual manual){
		//Find a manual using its serial number and then try to decrease it quantity from the library
		if (manual.increaseQtd(this.findManual(manual.getSerialNumber()))){
			manual.increaseQtd(manual);
			manual.setAvailable(true);
			return true;
		}
		return false;
	}

	public ArrayList<LoanableManual> getLibrary() {
		return library;
	}

	public void setLibrary(ArrayList<LoanableManual> library) {
		this.library = library;
	}
	
	//Generates next serial number for new book
	public String serialize(){
		String serial = "1";
		while (this.containsSerial(serial)){
			serial = String.valueOf(Integer.parseInt(serial) + 1);
		}
		return serial;
	}
	
	//Generates next serial number for new book
	public boolean containsSerial(String serial){
		for (LoanableManual m: this.library){
			if (m.getSerialNumber().equals(serial)){
				return true;
			}
		}
		return false;
	}
	
	//Sorting
	public void sort(){
	Collections.sort(this.library, new Comparator<LoanableManual>() {
	        @Override
	        public int compare(LoanableManual l1,  LoanableManual l2)
	        {

	        	if(Integer.parseInt(l1.getSerialNumber()) > Integer.parseInt(l2.getSerialNumber())) {
                    return 1;
                }
                else if(Integer.parseInt(l2.getSerialNumber()) > Integer.parseInt(l1.getSerialNumber())) {
                    return -1;
                }
                
                return 0;
	        }
	    });
	}
	
}
