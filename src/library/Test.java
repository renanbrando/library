package library;

public class Test {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	
	public Test(LoanableLibrary library){
		LoanableManual m = new LoanableManual();
		
		m.setSerialNumber("1");
		m.setTitle("The Eight");
		m.setAuthor("Katherine Neville");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();
	
		m.setSerialNumber("2");
		m.setTitle("The Fire");
		m.setAuthor("Katherine Neville");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

		m.setSerialNumber("3");
		m.setTitle("The Book Thief");
		m.setAuthor("Markus Zusak");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

		m.setSerialNumber("4");
		m.setTitle("Divergent");
		m.setAuthor("Veronica Roth");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

		m.setSerialNumber("5");
		m.setTitle("The Hunger Games");
		m.setAuthor("Suzanne Collins");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("6");
		m.setTitle("House of Night");
		m.setAuthor("P.C.Cast");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("7");
		m.setTitle("Elphame's Choice");
		m.setAuthor("P.C.Cast");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("8");
		m.setTitle("Harry Potter");
		m.setAuthor("J.K. Rowling");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("9");
		m.setTitle("Breghid's Quest");
		m.setAuthor("P.C.Cast");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("10");
		m.setTitle("Wuthering Heights");
		m.setAuthor("Emily Bronte");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("11");
		m.setTitle("Red Pyramid");
		m.setAuthor("Rick Riordan");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("12");
		m.setTitle("The Throne of Fire");
		m.setAuthor("Rick Riordan");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);

		m = new LoanableManual();
		
		m.setSerialNumber("13");
		m.setTitle("The Serpent's Shadow");
		m.setAuthor("Rick Riordan");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();
		
		m.setSerialNumber("14");
		m.setTitle("Cathing Fire");
		m.setAuthor("Suzanne Collins");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

		m.setSerialNumber("15");
		m.setTitle("Mockingjay");
		m.setAuthor("Suzanne Collins");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

		m.setSerialNumber("16");
		m.setTitle("Insurgent");
		m.setAuthor("Veronica Roth");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

		m.setSerialNumber("17");
		m.setTitle("Allegiant");
		m.setAuthor("Veronica Ruth");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();
	
		m.setSerialNumber("18");
		m.setTitle("Four");
		m.setAuthor("Veronica Ruth");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();
	
		m.setSerialNumber("19");
		m.setTitle("The Casual Vancancy");
		m.setAuthor("J.K. Rowling");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

		m.setSerialNumber("20");
		m.setTitle("The Night Circus");
		m.setAuthor("Erin Morgenstern");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();
	
		m.setSerialNumber("21");
		m.setTitle("Scarlet");
		m.setAuthor("Raiana Porteles");
		m.setBorrower("Renan");
		m.setAvailable(true);
		m.setQtd(1);
		library.addManual(m);
		
		m = new LoanableManual();

	}
}
