package library;

import javax.swing.JOptionPane;

public class JRemove {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	private LoanableManual result;
	private String search;
	
	public JRemove(LoanableLibrary library) {
		// TODO Auto-generated constructor stub
		this.removeManual();
		this.removeManual(library);
	}
	
	public void removeManual(){
		this.search = JOptionPane.showInputDialog(null, "Type the book's name:", "Remove", JOptionPane.DEFAULT_OPTION);
	}
	
	public void removeManual(LoanableLibrary library){
		Object[] books = library.searchManuals(this.search);
	    
	    this.result = (LoanableManual) JOptionPane.showInputDialog(null, "Listing all books...", "Books", JOptionPane.QUESTION_MESSAGE,
	        null, books, "Books");
	    
	    if (this.result != null){
	    	int option = JOptionPane.showConfirmDialog(null, "Are you sure do you wish to remove " + result.getTitle() + " from the library?" );
	    	if(option == JOptionPane.OK_OPTION)
	    		library.removeManual(result);	
	    		
	    }
	    else{
	    	JOptionPane.showMessageDialog(null, "Book not selected!");
	    }

	}
}
