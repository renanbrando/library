package library;

import java.io.FileNotFoundException;

import javax.swing.JOptionPane;

public class JLibrary extends JOptionPane{
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	private char selection;
	private LoanableLibrary library;
	private FileHandler data;
	
	//Contructor Method
	public JLibrary(){
		library = new LoanableLibrary();
		//new Test(library);
		data = new FileHandler();
		data.readFile(library);
		this.run();
	}
	
	//Selection getter and setter
	public int getSelection() {
		return selection;
	}
	public void setSelection(char selection) {
		this.selection = selection;
	}
	
	// Makes sure that selection will not be empty
	public String secureSelection(String s){
		if (s.isEmpty()){
			return "I";
		}
		return s;
	}
	
	//Running the menu and returning the option selected
	public void runMenu(){
		String input = JOptionPane.showInputDialog(null,
				"                   Choose an option:     \n"
				+ " 1 - Add a manual                      \n"
				+ " 2 - Remove a manual                   \n"
				+ " 3 - Edit a manual                     \n"
				+ " 4 - Find a manual                     \n"
				+ " 5 - Borrow Manual                     \n"
				+ " 6 - Return Manual                     \n"
				+ " 7 - Display all manuals on loan       \n"
				+ " 8 - Display all manuals not on loan   \n"
				+ " E - Exit                              \n"
				+ " A - About this app                    \n", "Library", JOptionPane.PLAIN_MESSAGE);
		//Handles cancel or exit button
		if (input == null)
			System.exit(ABORT);
		input = this.secureSelection(input);
		this.selection = Character.toUpperCase(input.charAt(0));
	}
	
	//Main loop of the app
	public void run(){
		while (this.selection != 'E'){
			//Menus selection
			this.runMenu();
			//Switch menu options with var selection
			switch (selection) {
			//Add a manual
			case '1':
				new JAdd(this.library);
				break;
			//Remove a manual
			case '2':
				new JRemove(this.library);
				break;
			//Edit a manual
			case '3':
				new JEdit(this.library);
				break;
			//Find a Manual
			case '4':
				new JFind(this.library);
				break;
			//Borrow a manual
			case '5':
				new JBorrow(this.library);
				break;
			//Return a manual
			case '6':
				new JReturn(this.library);
				break;
			//Display all manuals on loan
			case '7':
				new JList(this.library, false);
				break;
			//Display all manuals not on loan
			case '8':
				new JList(this.library, true);
				break;
			//About
			case 'A':
				JOptionPane.showMessageDialog(null, "Assignment II - Library\nCreated by Renan Ribeiro Brando.", "About", JOptionPane.INFORMATION_MESSAGE);
				break;
			//Exit
			case 'E':
				try {
					data.writesFile(library);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Thank you for using our library.", "Bye", JOptionPane.INFORMATION_MESSAGE);
				System.exit(ABORT);
				break;
			//Default error message
			default:
				JOptionPane.showMessageDialog(null, "Error, type a valid option!", "Invalid Option", JOptionPane.WARNING_MESSAGE);
				break;
			}
		
		}
	}

	
}
