package library;
import java.util.Scanner;

public class Manual implements IManual {
	private String serialNumber;
	private String title;
	private String author;
	
	//Constructor method
	Manual(){
		this.setSerialNumber("???????");
		this.setTitle("Untitled");
		this.setAuthor("Unknown");
	}
	
	//Alternative constructor method
	Manual(String serialNumber, String title, String author){
		this.setSerialNumber(serialNumber);
		this.setTitle(title);
		this.setAuthor(author);
	}
	
	//Serial Number getter and setter
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	//Title getter and setter
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	//Author getter and setter
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	//Print Method
	public void printManual(String serial){
		System.out.println("Title: " + this.getTitle());
		System.out.println("Author: " + this.getAuthor());
	}
	
	//toString method
	@Override
	public String toString() {
		String manual = "";
		manual += "Serial Number: " + this.serialNumber + "\n";
		manual += "Title: " + this.title + "\n";
		manual += "Author: " + this.author + "\n";
		
		return manual;
	}
	
	//Ask Method
	public Manual ask() {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		System.out.println("What is the manual's serial number?");
		this.serialNumber = scan.nextLine();
		System.out.println("What is the manual's title?");
		this.title = scan.nextLine();
		System.out.println("Who is the anual's author?");
		this.author = scan.nextLine();
		return new Manual(this.serialNumber,this.title,this.author);
	}
	
	//Validates the manual
	public boolean validatesManual(Manual manual) {
		// TODO Auto-generated method stub
		if (manual.getSerialNumber().isEmpty())
			return false;
		else if (manual.getSerialNumber().length() > 6)
			return false;
		return true;
	}
}
