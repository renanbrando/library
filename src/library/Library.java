package library;
import java.util.ArrayList;

public class Library implements ILibrary {
	public ArrayList<Manual> library = new ArrayList<Manual>();

	//Library getter and setter
	public ArrayList<Manual> getLibrary() {
		return library;
	}
	public void setLibrary(ArrayList<Manual> library) {
		this.library = library;
	}
	
	//Get a manual from the library
	public Manual findManual(String serial){
		// TODO Auto-generated method stub
		for (Manual m : this.library){
			if (m.getSerialNumber().equals(serial))
				return m;
		}
		return new Manual();
	}

	//Add a manual
	public void addManual(Manual manual) {
		// TODO Auto-generated method stub
		this.library.add(manual);
	}
	
	//Remove a manual
	public boolean removeManual(Manual manual) {
		this.library.remove(manual);
		return true;
	}

	//List all manuals
	public void listAllManuals() {
		// TODO Auto-generated method stub
		int i = 1;
		for (Manual m: this.library){
			System.out.println("---------Manual number "+i+"-----------");
			System.out.println("Serial: " + m.getSerialNumber());
			System.out.println("Name: " + m.getTitle());
			System.out.println("Name: " + m.getAuthor());
			i++;
		}
	}

	//Return manual like a string
	public String toString(Manual manual) {
		// TODO Auto-generated method stub
		String m = null;
		m = "Serial: " + manual.getSerialNumber() + "\n";
		m += "Title: " + manual.getTitle() + "\n";
		m += "Author: " + manual.getAuthor();
		return m;
	}
	
	//Validates entry
	public boolean validadesEntry(Manual manual) {
		// TODO Auto-generated method stub
		for (Manual m: this.library){
			if (manual.getSerialNumber().equals(m.getSerialNumber())){
				System.out.println("The book you are trying to add is not valid or already is in the library!");
				return false;
			}
		}
		this.addManual(manual);
		System.out.println("Book added successfully!");
		return true;
	}
	
}
