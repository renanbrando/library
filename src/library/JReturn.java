package library;

import javax.swing.JOptionPane;

public class JReturn {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	private LoanableManual result;
	private String search;
	
	public JReturn(LoanableLibrary library){
		// TODO Auto-generated constructor stub
		this.returnManual();
		this.returnManual(library);
	}
	
	public void returnManual(){
		this.search = JOptionPane.showInputDialog(null, "Type the book's name:", "Remove", JOptionPane.DEFAULT_OPTION);
	}
	
	public void returnManual(LoanableLibrary library){
		Object[] books = library.searchManuals(search);
	    
	    this.result = (LoanableManual) JOptionPane.showInputDialog(null, "Listing all books...", "Books", JOptionPane.QUESTION_MESSAGE,
	        null, books, "Books");
	    
	    if (this.result != null){
	    	int option = JOptionPane.showConfirmDialog(null, "Are you sure do you wish to return " + result.getTitle() + " from the library?" );
	    	if(option == JOptionPane.OK_OPTION){
	    		result.increaseQtd(result);
	    		JOptionPane.showMessageDialog(null, "Manual returned!");
	    	}
	    		
	    }
	    else{
	    	JOptionPane.showMessageDialog(null, "Book not selected!");
	    }

	}
}
