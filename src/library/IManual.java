package library;
public interface IManual {
		
	//Print Method
	public void printManual(String serial);
	
	//Validates the manual
	public boolean validatesManual(Manual manual);
	
	//Override toString
	public String toString();
	
	//Ask method for the user
	public Manual ask();
	
}





