package library;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class JAdd {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	private LoanableManual book;
	private JTextField title;
	private JTextField author;
	private JTextField borrower;
	private JTextField quantity;
	
	public JAdd(LoanableLibrary library) {
		// TODO Auto-generated constructor stub
		title = new JTextField();
		author = new JTextField();
		borrower = new JTextField();
		quantity = new JTextField();
		this.addManual(library);
	}


	public void addManual(LoanableLibrary library){
	Object[] message = {
			    "Title:", this.title,
			    "Author:", this.author,
			    "Borrower:", this.borrower,
			    "Quantity:", this.quantity
			};
	int option = JOptionPane.showConfirmDialog(null, message, "Add Book", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
			if (this.checkFields(this.title.getText(), this.author.getText(), this.borrower.getText(), this.quantity.getText())){
				book = new LoanableManual();
				book.setSerialNumber(String.valueOf(library.serialize()));
				book.setTitle(this.title.getText());
				book.setAuthor(this.author.getText());
				book.setAvailable(true);
				book.setBorrower(this.borrower.getText());
				book.setQtd(Integer.parseInt(this.quantity.getText()));
				library.addManual(this.book);
				this.add();
				this.clearManual();
			}
			else
				JOptionPane.showMessageDialog(null, "Fill in the fields properly!");
		}
	}
	
	public void add(){
		JOptionPane.showMessageDialog(null, "Book Added Successfully\n" +
										"Title: "+ book.getTitle() + "\n" +
										"Author: "+ book.getAuthor() + "\n" +
										"Available: " + book.isAvailable() + "\n" +
										"Borrower: "+ book.getBorrower() + "\n" +
										"Quantity: " + book.getQtd() , "Added", JOptionPane.DEFAULT_OPTION);
	}
	
	public void clearManual(){
		this.book = new LoanableManual();
	}
	
	public boolean checkFields(String title, String author, String borrower, String quantity){
		if (title.isEmpty() || author.isEmpty() || borrower.isEmpty() || quantity.isEmpty() || !this.isNumeric(quantity))
			return false;
		return true;
	}
	
	//Check if is a number
	public boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}

}
