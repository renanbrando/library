package library;

import javax.swing.JOptionPane;

public class JList {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	
	public JList(LoanableLibrary library, boolean loan) {
		// TODO Auto-generated constructor stub
		this.listManuals(library, loan);
	}
	
	public void listManuals(LoanableLibrary library){
		Object[] books = library.getLibrary().toArray();

	    JOptionPane.showInputDialog(null, "Listing all books...", "Books", JOptionPane.DEFAULT_OPTION,
	        null, books, "Books");
	}
	
	public void listManuals(LoanableLibrary library, boolean loan){
		LoanableLibrary l = new LoanableLibrary();
		
		for (LoanableManual lm : library.getLibrary()){
			if (lm.isAvailable() == loan)
				l.addManual(lm);
		}
		
		Object[] books = l.getLibrary().toArray();

	    JOptionPane.showInputDialog(null, "Listing all books " + (loan ? "available..." : "unavailable..."), "Books", JOptionPane.DEFAULT_OPTION,
	        null, books, "Books");
	}
}
