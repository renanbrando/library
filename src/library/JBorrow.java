package library;

import javax.swing.JOptionPane;

public class JBorrow {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	private LoanableManual result;
	private String search;
	
	public JBorrow(LoanableLibrary library){
		// TODO Auto-generated constructor stub
		this.borrowManual();
		this.borrowManual(library);
	}
	
	public void borrowManual(){
		this.search = JOptionPane.showInputDialog(null, "Type the book's name:", "Remove", JOptionPane.DEFAULT_OPTION);
	}
	
	public void borrowManual(LoanableLibrary library){
		Object[] books = library.searchManuals(this.search);
	    
	    this.result = (LoanableManual) JOptionPane.showInputDialog(null, "Listing all books...", "Books", JOptionPane.QUESTION_MESSAGE,
	        null, books, "Books");
	    
	    if (this.result != null){
	    	int option = JOptionPane.showConfirmDialog(null, "Are you sure do you wish to borrow " + result.getTitle() + " from the library?" );
	    	if(option == JOptionPane.OK_OPTION){
	    		if (result.getQtd() > 0){
	    			library.borrowManual(result);
	    			JOptionPane.showMessageDialog(null, "Manual borrowed!");
	    		}
	    		else {
	    			JOptionPane.showMessageDialog(null, "Manual cannot be borrowed!");
	    		}
	    	}
	    		
	    }
	    else{
	    	JOptionPane.showMessageDialog(null, "Book not selected!");
	    }

	}
}
