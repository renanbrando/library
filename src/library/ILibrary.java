package library;
public interface ILibrary {
	
	//Add a manual
	public void addManual(Manual manual);
	
	//List all manuals
	public void listAllManuals();
	
	//Find a manual by its serial
	public Manual findManual(String serial);
	
	//Validates entry
	public boolean validadesEntry(Manual manual);
	
}




