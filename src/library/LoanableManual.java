package library;
import java.util.Scanner;


public class LoanableManual extends Manual {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	
	public boolean available;
	public String borrower;
	private int qtd;
	
	
	public LoanableManual() {
		// TODO Auto-generated constructor stub
		super();
		this.available = false;
		this.borrower = "Unknown";
		this.qtd = 0;
	}
	
	public LoanableManual(String serialNumber, String title, String author, boolean available, String borrower, int qtd) {
		// TODO Auto-generated constructor stub
		this.setSerialNumber(serialNumber);
		this.setTitle(title);
		this.setAuthor(author);
		this.setAvailable(available);
		this.setBorrower(borrower);
		this.setQtd(qtd);
	}
	
	//Available getter and setter
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	//Borrower getter and setter
	public String getBorrower() {
		return borrower;
	}
	public void setBorrower(String borrower) {
		this.borrower = borrower;
	}
	
	//Qtd getter and setter
	public int getQtd() {
		return qtd;
	}
	public void setQtd(int qtd) {
		this.qtd = qtd;
	}
	
	//Auxiliar methods for manual manipulation
	public boolean decreaseQtd(LoanableManual lm){
		if (lm.qtd<1){
			return false;
		}
		
		lm.qtd--;
		return true;
	}
	
	public boolean increaseQtd(LoanableManual lm){
		lm.qtd += 1;
		return true;
	}
	
	@Override
	//Print Method
		public void printManual(String serial){
			System.out.println("Title: " + this.getTitle());
			System.out.println("Author: " + this.getAuthor());
			System.out.println("Available: " + this.isAvailable());
			System.out.println("Copies: " + this.getQtd());
			System.out.println("Borrower: " + this.getBorrower());
		}
		
	//toString method
	public String toString(LoanableManual lm){
		String manual = "";
		manual += "Serial Number: " + lm.getSerialNumber() + "\n";
		manual += "Title: " + lm.getTitle() + "\n";
		manual += "Author: " + lm.getAuthor() + "\n";
		manual += "Copies: " + lm.getQtd() + "\n";
		manual += "Available: " + (lm.isAvailable() ? "Yes" : "No") + "\n";
		manual += "Borrower: " + lm.getBorrower() +"\n";
		return manual;
	}
		
		//Ask Method
		public LoanableManual ask() {
			@SuppressWarnings("resource")
			LoanableManual temp = new LoanableManual();
			Scanner scan = new Scanner(System.in);
			System.out.println("What is the manual's serial number?");
			temp.setSerialNumber(scan.nextLine());
			System.out.println("What is the manual's title?");
			temp.setTitle(scan.nextLine());
			System.out.println("Who is the manual's author?");
			temp.setAuthor(scan.nextLine());
			temp.setAvailable(true);
			temp.increaseQtd(temp);
			return temp;
		}
		
		//Validates the manual
		public boolean validatesManual(LoanableManual manual) {
			// TODO Auto-generated method stub
			if (manual.getSerialNumber().isEmpty())
				return false;
			else if (manual.getSerialNumber().length() > 6)
				return false;
			return true;
		}
}
