package library;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;


public class FileHandler {
	/*
	 author Renan Ribeiro Brando ID:21266944
	 */
	
	private File file;
	private FileWriter writer;
	private FileReader reader;
	private String separator;
	
	public FileHandler() {
	// TODO Auto-generated constructor stub
		this.separator = "<>";
		this.file = new File("data.txt");
		this.createFile();
	}
	
	//Creates file if necessary
	public void createFile(){
		try {
			if (!this.file.exists()){
				JOptionPane.showMessageDialog(null, "Creating file...", "Creating file", JOptionPane.INFORMATION_MESSAGE);
				this.file.createNewFile();
			}
			else{
				JOptionPane.showMessageDialog(null, "Loading file...", "Loading file", JOptionPane.INFORMATION_MESSAGE);
			}
			this.writer = new FileWriter(this.file, true);
			this.reader = new FileReader(this.file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "File could not be created or loaded!", "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	//Writes the the library data to the file when the library is closed
		public void writesFile(LoanableLibrary l) throws FileNotFoundException{
			PrintWriter writer = new PrintWriter(this.file);
		    try {
				for (LoanableManual m : l.getLibrary()){
					writer.write(m.getSerialNumber() + this.separator );
					writer.write(m.getTitle() + this.separator);
					writer.write(m.getAuthor() + this.separator);
					writer.write(m.getQtd() + this.separator);
					writer.write(m.isAvailable() + this.separator);
					writer.write(m.getBorrower() + "\n");
				}
		    }
			finally{
				writer.flush();
				writer.close();
			}
		}
		

	//Read the file loading all the data into the library
	public void readFile(LoanableLibrary l){
		LoanableManual m = new LoanableManual();
		String[] parts = new String[5];
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader("data.txt"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Error reading the file!", "Error", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
			    parts = line.split(this.separator);
			    m = new LoanableManual(parts[0], parts[1], parts[2], Boolean.parseBoolean(parts[4]), parts[5], Integer.parseInt(parts[3]));
			    l.addManual(m);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Error loading the file!", "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}
